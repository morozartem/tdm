package by.issoft.tdm.server.model;

import lombok.Data;

/**
 * Created 3/24/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
public class DataItemShort {
    String path;
    String user;
    long date;
    String name;

}
