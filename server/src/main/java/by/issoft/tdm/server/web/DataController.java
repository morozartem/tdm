package by.issoft.tdm.server.web;

import by.issoft.tdm.server.data.ManualDataStore;
import by.issoft.tdm.server.model.ManualDataItem;
import by.issoft.tdm.server.model.ManualDataKey;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URLDecoder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created 3/23/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@RestController
@RequestMapping("/data")
public class DataController {

    final ManualDataStore dataStore;
    Path basePath = Paths.get("C:\\Projects\\job\\tdm\\sample_data\\manual_data\\");

    public DataController(ManualDataStore dataStore) {
        this.dataStore = dataStore;
    }

    @PostMapping
    public void save(@RequestBody ManualDataItem dataItem) throws IOException {
        if (dataItem != null) {
            dataStore.putData(dataItem);
//            Path file = basePath.resolve(dataItem.getDataKey().getPath())
//                    .resolve(dataItem.getDataKey().getUser())
//                    .resolve(dataItem.getDataKey().getName() + ".json");
//            file.toFile().getParentFile().mkdirs();
//            Files.write(file, Base64Utils.decodeFromString(dataItem.getData()));
        }

    }

    @GetMapping(value = {"/keys", "/keys/{user}"})
    public List<ManualDataKey> getKeyList(@PathVariable(required = false) String user, @RequestParam(required = false, name = "p") String path) throws IOException {
        if (path != null) {
            path = URLDecoder.decode(path, "UTF-8");
        }
        return dataStore.getAll(path, user);
    }
}
