package by.issoft.tdm.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerApplication {

    public static void main(String[] args) {
        System.setProperty("configFile", "tdm1.json");
        SpringApplication.run(ServerApplication.class, args);
    }

}
