package by.issoft.tdm.server.data;

import by.issoft.tdm.data.DataStore;
import by.issoft.tdm.domain.data.MetaStore;
import by.issoft.tdm.server.model.ManualDataItem;
import by.issoft.tdm.server.model.ManualDataKey;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created 3/25/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Component
public class ManualDataStore {

    public static final String DATA_ID = "mdata";

    List<ManualDataKey> dataItems;

    public ManualDataStore() {
        System.setProperty("configFile", "tdm1.json");
    }

    private List<ManualDataKey> getDataKeys() {
        if (dataItems == null) {
            MetaStore store = DataStore.getInstance().getDataService().getMetaStore().get(DATA_ID);
            dataItems = store.getMetaMap().entrySet().stream().flatMap(e ->
                    e.getValue().entrySet().stream().map(k ->
                            new ManualDataKey(e.getKey(), k.getKey()).setFileDate(k.getValue().lastModified())
                    )
            ).collect(Collectors.toList());
        }
        return dataItems;
    }

    public List<ManualDataKey> getAll(String path, String user) {
        return getDataKeys().stream()
                .filter(m -> (path == null || path.equalsIgnoreCase(m.getPath()))
                        && (user == null || (user.equalsIgnoreCase(m.getUser()))))
                .sorted(Comparator.comparing(ManualDataKey::getName))
                .collect(Collectors.toList());

    }

    //TODO exeptions

    public String getData(ManualDataKey dataKey) {
        return DataStore.getInstance().with(DATA_ID, dataKey.getName()).with(dataKey.getKey()).get();
    }

    public void putData(ManualDataItem dataItem) {
        DataStore.getInstance().store(DATA_ID, dataItem.getDataKey().getName(), dataItem.getDataKey().getKey(), dataItem.getData());
    }

    public void deleteData(ManualDataKey dataKey) {
        DataStore.getInstance().remove(DATA_ID, dataKey.getName(), dataKey.getKey());
    }
}
