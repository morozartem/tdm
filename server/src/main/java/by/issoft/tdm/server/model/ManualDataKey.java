package by.issoft.tdm.server.model;

import by.issoft.tdm.domain.data.DFKey;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Created 3/25/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ManualDataKey {

    String path;
    String user;
    String name;
    long fileDate;

    public ManualDataKey(String path, String user, String name) {

        this.path = path;
        this.user = user;
        this.name = name;
    }

    public ManualDataKey(String name, DFKey key) {
        this.name = name;
        this.path = key.first();
        this.user = key.second();
    }

    @JsonIgnore
    public DFKey getKey() {
        return DFKey.build(path, user);
    }


}