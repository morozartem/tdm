package by.issoft.tdm.api;

import by.issoft.tdm.domain.config.NGeneratorConfig;
import by.issoft.tdm.domain.generator.GenFileResult;
import by.issoft.tdm.domain.generator.GenResults;
import by.issoft.tdm.domain.schema.SchemaItem;

import java.util.Map;

/**
 * Created 3/17/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public interface FileGenerator {

    void init(NGeneratorConfig config);

    Map<String, GenFileResult> generateFile(String name, SchemaItem item, GenResults genResults);
}
