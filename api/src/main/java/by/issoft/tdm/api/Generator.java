package by.issoft.tdm.api;

import by.issoft.tdm.domain.generator.GenFileChanges;

import java.io.IOException;

/**
 * Created 16.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public interface Generator {

    void regenerate(GenFileChanges changes) throws IOException;
}
