package by.issoft.tdm.api;

import by.issoft.tdm.domain.data.DFInfo;
import by.issoft.tdm.domain.enums.ChangeType;

import java.nio.file.Path;

/**
 * Created 16.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public interface DataChangeListener {

    void fileChange(DFInfo info, ChangeType changeType);

    void dirChange(Path path, ChangeType changeType);
}
