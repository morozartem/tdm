package by.issoft.tdm.api;

import by.issoft.tdm.domain.config.NGeneratorConfig;
import by.issoft.tdm.domain.generator.GenResults;

/**
 * Created 3/17/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public interface GlobalGenerator {

    void init(NGeneratorConfig config);

    void generate(GenResults genResults);
}
