package by.issoft.tdm.api.data;

import by.issoft.tdm.domain.data.DH;

import java.util.Map;

/**
 * Created 4/3/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public interface DataProcessor {

    void prepare(String id, String data, DH holder, Map<String, Object> context);
}
