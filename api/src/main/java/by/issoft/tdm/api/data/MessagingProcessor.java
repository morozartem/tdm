package by.issoft.tdm.api.data;

/**
 * Created 4/3/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public interface MessagingProcessor extends DataProcessor {
}
