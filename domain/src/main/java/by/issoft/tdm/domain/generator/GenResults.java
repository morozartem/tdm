package by.issoft.tdm.domain.generator;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created 17.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
public class GenResults implements Serializable {

    Map<String, GenFileResult> results = new HashMap<>();


}
