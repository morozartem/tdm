package by.issoft.tdm.domain.enums;

/**
 * Created 09.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public enum TDMMode {

    DF, GEN, UI
}
