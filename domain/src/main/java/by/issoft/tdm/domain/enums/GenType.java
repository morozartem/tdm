package by.issoft.tdm.domain.enums;

/**
 * Created 3/27/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public enum GenType {
    DATA, LOCATORS, MESSAGES
}
