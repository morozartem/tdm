package by.issoft.tdm.domain.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.nio.file.Path;
import java.util.Map;

/**
 * Created 09.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
public class TDMConfig {
    //    List<String> structure;
//    String structureType;
//    String dataPath;
//    @JsonIgnore
//    Path realDataPath;
    int fsListenerInterval;
    String defaultLocale;
    //    Map<String, FileConfig> data;
//    GeneratorConfig gen;
    @JsonProperty("generators")
    Map<String, NGeneratorConfig> genConfig;
    @JsonIgnore
    Path configFile;
}
