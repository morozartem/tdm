package by.issoft.tdm.domain.generator;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created 16.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
public class FakeMeta {
    Map<String, List<String>> fakeFuncs = new HashMap<>();
    List<String> customVars = new ArrayList<>();
}
