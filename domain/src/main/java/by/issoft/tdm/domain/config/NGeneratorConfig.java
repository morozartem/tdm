package by.issoft.tdm.domain.config;

import by.issoft.tdm.domain.enums.GenType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

/**
 * Created 16.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
public class NGeneratorConfig {

    @JsonIgnore
    String id;

    GenType type;

    String dataPath;
    @JsonIgnore
    Path realDataPath;
    List<String> structure;
    String structureType;
    @JsonProperty("itemGenerator")
    String itemGeneratorClass;
    @JsonProperty("globalGenerators")
    List<String> generatorsClassList;
    Map<String, FileConfig> data;
    String generatePath;
    @JsonIgnore
    Path realGeneratePath;
    Map<String, Object> options;
}
