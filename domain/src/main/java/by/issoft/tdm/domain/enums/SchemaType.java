package by.issoft.tdm.domain.enums;

/**
 * Created 06.02.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public enum SchemaType {
    OBJECT, STRING, ARRAY, INTEGER, BOOLEAN, DECIMAL
}
