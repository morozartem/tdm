package by.issoft.tdm.domain.generator;

import lombok.Data;

import java.util.List;

/**
 * Created 10.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
public class GenFileChanges {
    List<GenFileMeta> delete;
    List<GenFileMeta> regenerate;

    public boolean isEmpty() {
        return delete == null && regenerate == null;
    }
}
