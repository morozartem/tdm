package by.issoft.tdm.domain.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created 09.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DFKey implements Cloneable {

    List<String> heritageKey;

    public String first() {
        return get(0);
    }

    public String second() {
        return get(1);
    }

    public String third() {
        return get(2);
    }

    public void add(String value) {
        if (heritageKey == null) {
            heritageKey = new ArrayList<>();
        }
        heritageKey.add(value);
    }

    protected String get(int index) {
        try {
            return heritageKey.get(index);
        } catch (Exception ex) {
            //
        }
        return null;
    }

    //TODO REFACTOR
    public static DFKey build(Path path, Path root, String fsMode) {
        if ("folders".equalsIgnoreCase(fsMode)) {
            Path diff = root.relativize(path);
            if (diff.getNameCount() == 1) {
                return null;
            }
            List<String> heritage = new ArrayList<>();
            for (int i = 0; i < diff.getNameCount() - 1; i++) {
                heritage.add(diff.getName(i).toString());
            }
            return new DFKey(heritage);
        } else {
            throw new NotImplementedException();
        }
    }

    public static DFKey build(String... keys) {
        return new DFKey(Arrays.asList(keys));
    }
}
