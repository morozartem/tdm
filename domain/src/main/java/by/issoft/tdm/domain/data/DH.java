package by.issoft.tdm.domain.data;

import by.issoft.tdm.domain.enums.FakerMode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * Created 4/3/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DH {

    Map<String, Object> customVars;
    FakerMode mode;
    DFKey key;
    List<Object> vars;

    public DH(DFKey key) {
        this.key = key;
    }

    public DH(FakerMode mode) {
        this.mode = mode;
    }

    public DH(Map<String, Object> customVars) {
        this.customVars = customVars;
    }

    public DH(List<Object> vars) {
        this.vars = vars;
    }

    public DH with(DFKey key) {
        this.key = key;
        return this;
    }

    public DH with(Map<String, Object> customVars) {
        this.customVars = customVars;
        return this;
    }

    public DH with(FakerMode mode) {
        this.mode = mode;
        return this;
    }

    public DH with(List<Object> vars) {
        this.vars = vars;
        return this;
    }
}
