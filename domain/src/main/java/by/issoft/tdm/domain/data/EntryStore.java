package by.issoft.tdm.domain.data;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Created 17.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
public class EntryStore {

    Map<String, Map<DFKey, String>> entryMap;

    public EntryStore() {
        entryMap = new HashMap<>();
    }

    public void clear() {
        entryMap.clear();
    }

    public String getData(String name, DFKey dfKey) {
        try {
            return entryMap.get(name).get(dfKey);
        } catch (Exception ex) {
            return null;
        }
    }

    public void put(String name, DFKey key, String data) {
        Map<DFKey, String> tmp = entryMap.get(name);
        if (tmp == null) {
            tmp = new HashMap<>();
            entryMap.put(name, tmp);
        }
        tmp.put(key, data);
    }

    public void remove(String name, DFKey key) {
        try {
            entryMap.get(name).remove(key);
        } catch (Exception ex) {
            //
        }
    }

    public void remove(String name) {
        try {
            entryMap.remove(name);
        } catch (Exception ex) {
            //
        }
    }
}
