package by.issoft.tdm.domain.generator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Path;

/**
 * Created 10.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenFileMeta implements Serializable {
    String name;
    String filePath;
    long size;
    long lastModified;


    @JsonIgnore
    @EqualsAndHashCode.Exclude
    transient File f;

    public static GenFileMeta build(String name, Path root, File f) {
        return new GenFileMeta(name, root.relativize(f.toPath()).toString(),
                f.length(), f.lastModified(), f);
    }
}