package by.issoft.tdm.domain.generator;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created 18.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
public class GenerationResults implements Serializable {
    String messageClass;
    Map<String, List<GenFileMeta>> filesMeta = new HashMap<>();
    Map<String, List<GenFileResult>> filesResults = new HashMap<>();

    public void addGenFileMeta(String id, GenFileMeta fileMeta) {
        List<GenFileMeta> metas = filesMeta.get(id);
        if (metas == null) {
            metas = new ArrayList<>();
            filesMeta.put(id, metas);
        }
        metas.add(fileMeta);
    }

    public void addGenFileMeta(String id, List<GenFileMeta> fileMeta) {
        List<GenFileMeta> metas = filesMeta.get(id);
        if (metas == null) {
            metas = new ArrayList<>();
            filesMeta.put(id, metas);
        }
        metas.addAll(fileMeta);
    }

    public void addGenFileResult(String id, GenFileResult fileResult) {
        List<GenFileResult> results = filesResults.get(id);
        if (results == null) {
            results = new ArrayList<>();
            filesResults.put(id, results);
        }
        results.add(fileResult);
    }

    public void addGenFileResult(String id, List<GenFileResult> fileResult) {
        List<GenFileResult> results = filesResults.get(id);
        if (results == null) {
            results = new ArrayList<>();
            filesResults.put(id, results);
        }
        results.addAll(fileResult);
    }

    public List<GenFileMeta> getMetas(String id) {
        return filesMeta.get(id);
    }

    public List<GenFileResult> getResults(String id) {
        return filesResults.get(id);
    }


}
