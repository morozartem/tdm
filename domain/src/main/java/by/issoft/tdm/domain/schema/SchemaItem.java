package by.issoft.tdm.domain.schema;

import by.issoft.tdm.domain.enums.SchemaType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.*;

/**
 * Created 06.02.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
@Accessors(chain = true)
public class SchemaItem implements Serializable {
    String name;
    String dataName;

    SchemaType type;
    SchemaType arrayItemType;

    //@JsonBackReference
    List<SchemaItem> itemList;
    //@JsonManagedReference()
    @ToString.Exclude
    SchemaItem parent;

    Map<String, String> properties = new HashMap<>();
    Set<String> childImports = new HashSet<>();

    public void addProperty(String key, String value) {
        if (properties == null) {
            properties = new HashMap<>();
        }
        properties.put(key, value);
    }

    @JsonIgnore
    public boolean isObject() {
        return SchemaType.OBJECT.equals(type);
    }

    @JsonIgnore
    public boolean isArray() {
        return SchemaType.ARRAY.equals(type);
    }

    @JsonIgnore
    public boolean isSimpleArray() {
        return isArray() && !SchemaType.OBJECT.equals(arrayItemType);
    }

    @JsonIgnore
    public boolean isComplexArray() {
        return isArray() && SchemaType.OBJECT.equals(arrayItemType);
    }

}
