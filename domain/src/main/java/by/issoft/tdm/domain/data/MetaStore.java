package by.issoft.tdm.domain.data;

import lombok.Data;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created 17.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
public class MetaStore {

    Map<String, Map<DFKey, File>> metaMap;

    public MetaStore() {
        metaMap = new HashMap<>();
    }

    public void clear() {
        metaMap.clear();
    }

    public File getDataFile(String name, DFKey dfKey) {
        try {
            return metaMap.get(name).get(dfKey);
        } catch (Exception ex) {
            return null;
        }
    }

    public void put(String name, DFKey key, File file) {
        Map<DFKey, File> tmp = metaMap.get(name);
        if (tmp == null) {
            tmp = new HashMap<>();
            metaMap.put(name, tmp);
        }
        tmp.put(key, file);
    }

    public void put(DFInfo dfInfo) {
        this.put(dfInfo.getName(), dfInfo.getKey(), dfInfo.getFile());
    }

    public void remove(String name, DFKey key) {
        try {
            metaMap.get(name).remove(key);
        } catch (Exception ex) {
            //
        }
    }

    public void remove(String name) {
        try {
            metaMap.remove(name);
        } catch (Exception ex) {
            //
        }
    }

}
