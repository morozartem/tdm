package by.issoft.tdm.domain.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.nio.file.Path;
import java.util.Map;

/**
 * Created 16.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
public class GeneratorConfig {
    String genPath;
    @JsonIgnore
    Path realGenPath;
    String tmpMetaPath;
    @JsonIgnore
    Path realTmpMetaPath;
    String lang;
    Map<String, Object> options;
}