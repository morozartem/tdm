package by.issoft.tdm.domain.generator;

import by.issoft.tdm.domain.schema.SchemaItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * Created 17.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class GenFileResult implements Serializable {

    String name;
    //String relativePath;

    SchemaItem schemaItem;

}
