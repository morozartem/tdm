package by.issoft.tdm.domain.enums;

/**
 * Created 16.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public enum FakerMode {

    SINGLETON, PROXY, THREAD
}
