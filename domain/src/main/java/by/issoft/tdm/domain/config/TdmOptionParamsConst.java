package by.issoft.tdm.domain.config;

/**
 * Created 16.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public class TdmOptionParamsConst {
    public static final String BASE_PACKAGE = "basePackage";
    public static final String USE_LOMBOK = "useLombok";
    public static final String GEN_DATAPROVIDER = "genDP";
}
