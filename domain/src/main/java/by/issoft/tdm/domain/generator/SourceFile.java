package by.issoft.tdm.domain.generator;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

/**
 * Created 13.02.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
@Accessors(chain = true)
@Slf4j
public class SourceFile {

    protected static List<Character> endChars = Arrays.asList('{', '}', '\n');

    protected StringBuilder stringBuilder;
    protected Path path;

    public SourceFile() {
        this.stringBuilder = new StringBuilder();
    }


    protected String processSourceBeforeWrite() {
        return stringBuilder.toString();
    }

    public void line(String s) {
        //s = s.trim();

        if (s.length() != 0 && !endChars.contains(s.charAt(s.length() - 1))) {
            s += ";";
        }
        stringBuilder.append(s + "\n");
    }

    public int indexOf(String str) {
        return stringBuilder.indexOf(str);
    }

    public void insert(int index, String string) {
        stringBuilder.insert(index, string);
    }

    public void writeFile() throws IOException {
        Files.createDirectories(path.getParent());
        Files.deleteIfExists(path);
        Files.createFile(path);
        try (FileWriter writer = new FileWriter(path.toFile(), false)) {
            writer.write(processSourceBeforeWrite());
        }
        log.trace(processSourceBeforeWrite());
    }
}
