package by.issoft.tdm.domain.config;

import lombok.Data;

import java.util.List;

/**
 * Created 16.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
public class FileConfig {
    List<String> keys;
    Object ui;
}