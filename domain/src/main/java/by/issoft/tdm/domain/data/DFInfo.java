package by.issoft.tdm.domain.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.File;
import java.nio.file.Path;

/**
 * Created 09.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class DFInfo {

    File file;
    String name;
    DFKey key;

    public static DFInfo build(Path p, Path root, String fsMode) {
        File f = p.toFile();
        String fileName = getFileNameWithoutExtension(f);
        //TODO !!!!FilenameUtils.getBaseName(p.toString());
        DFKey key = DFKey.build(p, root, fsMode);
        DFInfo info = new DFInfo(f, fileName, key);
        return info;
    }

    private static String getFileNameWithoutExtension(File file) {
        String fileName = "";

        try {
            if (file != null && file.exists()) {
                String name = file.getName();
                fileName = name.replaceFirst("[.][^.]+$", "");
            }
        } catch (Exception e) {
            e.printStackTrace();
            fileName = "";
        }

        return fileName;

    }
}
