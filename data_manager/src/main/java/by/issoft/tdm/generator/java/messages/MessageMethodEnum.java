package by.issoft.tdm.generator.java.messages;

/**
 * Created 3/31/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Deprecated
public enum MessageMethodEnum {
    EMPTY, FULL, KEY, VARS
}
