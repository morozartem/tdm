package by.issoft.tdm.generator;

import by.issoft.tdm.api.FileGenerator;
import by.issoft.tdm.api.GlobalGenerator;
import by.issoft.tdm.domain.config.NGeneratorConfig;
import by.issoft.tdm.domain.config.TDMConfig;
import by.issoft.tdm.domain.data.MetaStore;
import by.issoft.tdm.domain.generator.*;
import by.issoft.tdm.domain.schema.SchemaItem;
import by.issoft.tdm.utils.SchemaExtractor;
import by.issoft.tdm.utils.SerializeFileUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created 17.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Slf4j
public class BaseGenerator {

    List<String> generators;
    ObjectMapper mapper;
    SchemaExtractor extractor;
    FileGenerator fileGenerator;
    List<GlobalGenerator> globalGenerators;

    public BaseGenerator() {
        mapper = new ObjectMapper();
        generators = new ArrayList<>();
        extractor = new SchemaExtractor();
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        File f = new File("C:\\Projects\\rnd\\tdm\\tmd.info");
        GenerationResults results = new SerializeFileUtils<GenerationResults>().deserialize(f);
        System.out.println(results);
    }

    protected void initGenerators(NGeneratorConfig config) {
        //TODO implement plugins and may be reduce govnocode
//        fileGenerator = new JavaPojoGenerator();
//        fileGenerator.init(config);

        try {
            fileGenerator = (FileGenerator) Class.forName(config.getItemGeneratorClass()).newInstance();
            fileGenerator.init(config);
        } catch (Exception e) {
            e.printStackTrace();
        }
        globalGenerators = config.getGeneratorsClassList().stream().map(c -> {
            try {
                GlobalGenerator g = (GlobalGenerator) Class.forName(c).newInstance();
                g.init(config);
                return g;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }).collect(Collectors.toList());

//        JavaDataFactoryGenerator dataFactoryGenerator = new JavaDataFactoryGenerator();
//        dataFactoryGenerator.init(config);
//        globalGenerators = Arrays.asList(dataFactoryGenerator);

    }

    public GenResults regenerate(String id, MetaStore metaStore, NGeneratorConfig config, TDMConfig globalConfig, GenerationResults old, GenerationResults results) throws IOException {
        log.info("Start regenerating for " + id);
        initGenerators(config);
        Path root = config.getRealDataPath();
        //create current state
        //GenDataMeta current = new GenDataMeta();
//        current.setBrunchName("master");
        List<GenFileMeta> currentFiles = metaStore.getMetaMap().entrySet().stream().flatMap(entry ->
                entry.getValue().entrySet().stream()
                        .map(e -> GenFileMeta.build(entry.getKey(), root, e.getValue()))
        ).collect(Collectors.toList());

        results.addGenFileMeta(id, currentFiles);

        //get old state
//        Path projectPath = Paths.get("");
//        File fMeta = projectPath.resolve("tdmdata/" + id + "_meta.json").toFile();
//        File fResults = projectPath.resolve("tdmdata/" + id + "_gen.json").toFile();
//        //File f = config.getRealTmpMetaPath().toFile();
        List<GenFileMeta> oldFiles = old.getMetas(id);
        //readOldState(fMeta);
        //compare
        GenFileChanges genFileChanges = getMetaDiff(oldFiles, currentFiles);

        //regenerate changes and old schema
        if (!genFileChanges.isEmpty()) {
            //read old schemas
            GenResults oldResults = null;
            if (old.getResults(id) != null) {
                oldResults = new GenResults();
                oldResults.setResults(old.getResults(id).stream().collect(Collectors.toMap(GenFileResult::getName, e -> e)));
            }
            //readOldResults(fResults);

            GenResults res =
                    generate(genFileChanges, oldResults, config);
            results.addGenFileResult(id, new ArrayList<>(res.getResults().values()));

            //save new schema and new datameta
//            fMeta.getParentFile().mkdirs();
//            mapper.writeValue(fMeta, current);
            //new SerializeFileUtils<GenResults>().serialize(new File(fResults.getAbsolutePath()+"1"), results);
            //mapper.writeValue(fResults, results);

            //log.info("Meta written to "+fMeta.getAbsolutePath());
            return res;
        } else {
            log.info("NOTHING!");
        }
        return null;
    }

    protected GenResults generate(GenFileChanges genFileChanges, GenResults old, NGeneratorConfig config) {
        //regenerate all simple files
        if (genFileChanges.getDelete() != null) {
            genFileChanges.getDelete().forEach(genFileMeta -> new File(genFileMeta.getFilePath()).deleteOnExit());
        }

        Map<String, SchemaItem> meta = readNewSchemas(genFileChanges);
        final GenResults results = old == null ? new GenResults() : old;
        if (fileGenerator != null) {
            meta.entrySet().forEach(e -> {
                Map<String, GenFileResult> fileResults = fileGenerator.generateFile(e.getKey(), e.getValue(), results);
                results.getResults().putAll(fileResults);
            });
        }
        //generate all additional files
        if (globalGenerators != null) {
            globalGenerators.forEach(globalGenerator -> {
                globalGenerator.generate(results);
            });
        }
        return results;
    }

    protected Map<String, SchemaItem> readNewSchemas(GenFileChanges genFileChanges) {
        Map<String, List<GenFileMeta>> groupList = genFileChanges.getRegenerate().stream().collect(Collectors.groupingBy(GenFileMeta::getName));

        Map<String, SchemaItem> meta =
                groupList.entrySet().stream().collect(Collectors.toMap(e -> e.getKey(), e -> {
                    List<JsonNode> jsonNodes = e.getValue().stream().map(d -> {
                        try {
                            return mapper.readTree(d.getF());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            return null;
                        }
                    }).collect(Collectors.toList());
                    return extractor.getSchemaForNodes(jsonNodes, e.getKey());
                }));
        return meta;
    }

    //TODO doubleCheck
    protected GenFileChanges getMetaDiff(List<GenFileMeta> old, List<GenFileMeta> current) {
        GenFileChanges changes = new GenFileChanges();
        //get list that need to be removed
        if (old != null) {
            if (!old.equals(current)) {
                Map<String, GenFileMeta> oldFiles = old.stream()
                        .collect(Collectors.toMap(e -> e.getFilePath(), e -> e));

                Map<String, GenFileMeta> currentFiles = current.stream()
                        .collect(Collectors.toMap(e -> e.getFilePath(), e -> e));

                List<GenFileMeta> delete = oldFiles.entrySet().stream()
                        .filter(e -> !currentFiles.containsKey(e.getKey())).map(e -> e.getValue()).collect(Collectors.toList());
                //get list to regenerate
                List<GenFileMeta> regenerate = new ArrayList<>(current);
                regenerate.removeAll(old);
                regenerate = regenerate.stream().flatMap(e ->
                        current.stream().filter(l -> l.getName().equalsIgnoreCase(e.getName()))
                ).collect(Collectors.toList());

                log.debug("Should be deleted " + delete);
                log.debug("Should be regenerated " + regenerate);

                changes.setDelete(delete.isEmpty() ? null : delete);
                changes.setRegenerate(regenerate.isEmpty() ? null : regenerate);
            } else {
                log.debug("Metas are equal, nothing changed");
            }
        } else {
            changes.setRegenerate(current);
            log.debug("Should be regenerated " + current);
        }
        return changes;
    }


}
