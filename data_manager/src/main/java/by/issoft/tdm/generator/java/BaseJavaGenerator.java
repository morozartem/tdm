package by.issoft.tdm.generator.java;

import by.issoft.tdm.api.FileGenerator;
import by.issoft.tdm.api.GlobalGenerator;
import by.issoft.tdm.data.DataStore;
import by.issoft.tdm.domain.common.Pair;
import by.issoft.tdm.domain.config.NGeneratorConfig;
import by.issoft.tdm.domain.data.DFKey;
import by.issoft.tdm.domain.generator.SourceFile;
import by.issoft.tdm.domain.schema.SchemaItem;
import by.issoft.tdm.utils.DFKeyHelper;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;

import javax.annotation.Generated;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.util.*;

import static by.issoft.tdm.domain.config.TdmOptionParamsConst.BASE_PACKAGE;
import static by.issoft.tdm.domain.config.TdmOptionParamsConst.USE_LOMBOK;

/**
 * Created 18.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public abstract class BaseJavaGenerator implements FileGenerator, GlobalGenerator {

    protected static final String PACKAGE = "package";
    protected static final String FULL_NAME = "fullPackage";
    protected static final String IMPORT = "import ";

    protected static List<String> imports = Arrays.asList(Serializable.class.getCanonicalName(),
            Generated.class.getCanonicalName(),
            List.class.getCanonicalName(),
            IOException.class.getCanonicalName(),
            DataStore.class.getCanonicalName(),
            DFKey.class.getCanonicalName(),
            Map.class.getCanonicalName(),
            JsonProperty.class.getCanonicalName(),
            HashMap.class.getCanonicalName(),
            DFKeyHelper.class.getCanonicalName());

    protected static List<String> lombockImports = Arrays.asList(Data.class.getCanonicalName(),
            Accessors.class.getCanonicalName(), AllArgsConstructor.class.getCanonicalName(),
            NoArgsConstructor.class.getCanonicalName());

    protected Path basePath;
    protected String basePackage;
    protected Map<String, Object> options;
    protected NGeneratorConfig config;
    protected boolean useLombok;

    public void init(NGeneratorConfig config) {
        this.config = config;
        basePath = config.getRealGeneratePath();
        Assert.assertNotNull("Target dir from generator options is null!", basePath);
        options = config.getOptions();
        Assert.assertNotNull("Options from Java generator is null!", options);

        basePackage = options.get(BASE_PACKAGE).toString();
        Assert.assertNotNull("BasePackage from Java generation is null!", basePackage);
        for (String p : basePackage.split("\\.")) {
            basePath = basePath.resolve(p);
        }
        useLombok = (Boolean) options.get(USE_LOMBOK);
        afterInit();
    }

    protected void afterInit() {
    }

    protected Pair<String, Path> detectPackage(SchemaItem schemaItem) {
        String pack = basePackage;
        SchemaItem parent = schemaItem.getParent();
        List<String> parentsList = new ArrayList<>();
        if (parent == null) {
            //this is base class and all first level childs
            parentsList.add(schemaItem.getName().toLowerCase());
        } else {
            while (parent != null) {
                String parentPack = parent.getName().toLowerCase();
                parentsList.add(parentPack);

                parent = parent.getParent();
            }
        }
        Path path = basePath;//TODO????
        if (!parentsList.isEmpty()) {
            Collections.reverse(parentsList);
            for (String parentPack : parentsList) {
                pack += "." + parentPack;
                path = path.resolve(parentPack);
            }
        }
        path = path.resolve(cap(schemaItem.getName()) + ".java");

        Pair<String, Path> result = new Pair<>(pack, path);
        schemaItem.addProperty(PACKAGE, result.getKey());
        schemaItem.addProperty(FULL_NAME, result.getKey() + "." + cap(schemaItem.getName()));
        schemaItem.addProperty("filePath", result.getValue().toAbsolutePath().toString());
        return result;
    }

    protected void addLombok(SourceFile sourceFile) {
        if ((Boolean) options.get(USE_LOMBOK)) {
            lombockImports.forEach(i -> sourceFile.line(IMPORT + i));
            sourceFile.line("@Data\n" +
                    // "@AllArgsConstructor\n" +
                    "@NoArgsConstructor\n" +
                    "@Accessors(chain = true)\n");
        }
    }

    protected String cap(String str) {
        return StringUtils.capitalize(str);
    }
}
