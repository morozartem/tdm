package by.issoft.tdm.generator.java;


import by.issoft.tdm.domain.generator.SourceFile;
import com.google.googlejavaformat.java.Formatter;
import com.google.googlejavaformat.java.FormatterException;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;

/**
 * Created 13.02.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Slf4j
public class JavaSourceFile extends SourceFile {

    public JavaSourceFile() {
        super();
        endChars = new ArrayList<>(endChars);
        endChars.add(';');
    }

    @Override
    protected String processSourceBeforeWrite() {
        try {
            return new Formatter().formatSource(stringBuilder.toString());
        } catch (FormatterException e) {
            System.out.println(stringBuilder.toString());
            log.error("Unable to format source file " + path, e);
        }
        return stringBuilder.toString();
    }
}
