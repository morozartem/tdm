package by.issoft.tdm.generator.java.locators;

import by.issoft.tdm.domain.enums.GenType;
import by.issoft.tdm.domain.generator.SourceFile;
import by.issoft.tdm.domain.schema.SchemaItem;
import by.issoft.tdm.generator.java.data.JavaPojoGenerator;
import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;

/**
 * Created 3/17/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Slf4j
public class LocatorJavaGenerator extends JavaPojoGenerator {

    boolean isLocatorClass;

    public static void main(String[] args) {
        System.out.println(new Faker().idNumber().valid());
    }

    protected void generateSimpleArrayField(SchemaItem schemaItem, SourceFile fileHolder) {
        //do nothing, cause arrays not supported in locators
    }

    @Override
    protected void afterInit() {
        useLombok = false;
        isLocatorClass = config.getType().equals(GenType.LOCATORS);
    }

    protected void appendField(String type, SchemaItem schemaItem, SourceFile fileHolder) {
        String name = schemaItem.getName();
        //CaseUtils.toCamelCase(schemaItem.getName(), false);
        fileHolder.line("@JsonProperty(\"" + schemaItem.getName() + "\")\n private " + type + " " + name);
        fileHolder.line("public " + type + " get" + cap(name) + "(){return this." + name + ";}");
        if ("String".equalsIgnoreCase(type) && isLocatorClass) {
            fileHolder.line("public org.openqa.selenium.By by" + cap(name) + "(){return gen.locators.L.get(this." + name + ");}"); //todo refactor packages
        }
        fileHolder.line("\n");
    }
}
