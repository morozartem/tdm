package by.issoft.tdm.generator.java.locators;

import by.issoft.tdm.config.ConfigService;
import by.issoft.tdm.domain.data.DH;
import by.issoft.tdm.domain.generator.GenFileResult;
import by.issoft.tdm.domain.generator.GenResults;
import by.issoft.tdm.domain.generator.SourceFile;
import by.issoft.tdm.domain.schema.SchemaItem;
import by.issoft.tdm.generator.java.BaseJavaGenerator;
import by.issoft.tdm.generator.java.JavaSourceFile;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.CaseUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created 18.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Slf4j
public class LClassGenerator extends BaseJavaGenerator {

    protected static List<String> lImports = new ArrayList<>(imports);

    @Override
    public Map<String, GenFileResult> generateFile(String name, SchemaItem item, GenResults genResults) {
        throw new UnsupportedOperationException("XXX");
    }

    protected String getClassName() {
        return "L";
    }

    @Override
    public void generate(GenResults genResults) {
        log.info("Will create {} {}", getClassName(), genResults.getResults());
        lImports.addAll(
                Arrays.asList(TypeReference.class.getCanonicalName(),
                        ObjectMapper.class.getCanonicalName(),
                        JsonProcessingException.class.getCanonicalName(),
                        ConfigService.class.getCanonicalName(),
                        DeserializationFeature.class.getCanonicalName(),
                        DH.class.getCanonicalName()));
        try {
            generateL(genResults);
        } catch (Exception ex) {
            //
            ex.printStackTrace();
        }
    }

    private GenFileResult generateL(GenResults genResults) throws IOException {
        SourceFile sourceFile = generateLHeader();
        genResults.getResults().values().forEach(e -> {
            SchemaItem schemaItem = e.getSchemaItem();
            addItemToL(schemaItem, sourceFile);
        });
        generateLFooter(sourceFile);
        sourceFile.writeFile();
        return null;
    }

    protected void addItemToL(SchemaItem schemaItem, SourceFile fileHolder) {
        String argName = schemaItem.getDataName();
        String mapName = argName + "Map";
        String funcName = CaseUtils.toCamelCase(argName, true);
        String fullClass = schemaItem.getProperties().get(FULL_NAME);
        String id = config.getId();
        fileHolder.line("static Map<DFKey, " + fullClass + "> " + mapName + " = new HashMap<>();");
        fileHolder.line(
                "public static " + fullClass + " " + funcName + "(DFKey key, Map<String, Object> customVars, List<Object> vars){\n" +
                        fullClass + " " + argName + " = " + mapName + ".get(key);\n" +
                        " if (" + argName + "==null){" +
                        "   try {\n" +
                        " DH dh = new DH();\n" +
                        "    dh.setKey(key);\n" +
                        "    dh.setCustomVars(customVars);\n" +
                        "    dh.setVars(vars);\n" +
                        "                " + argName + " = mapper.readValue(" +
                        "DataStore.getInstance().with(\"" + id + "\", \"" + argName + "\")" +
                        ".with(dh).get(), " + fullClass + ".class);\n" +
                        mapName + ".put(key, " + argName + ");" +
                        "   } catch (JsonProcessingException e) {\n" +
                        "       e.printStackTrace();\n" +
                        "   } " +
                        "   }\n" +
                        "return " + argName + ";" +
                        "}");

        fileHolder.line("public static " + fullClass + " " + funcName + "(){return "
                + getClassName() + "." + funcName + "(ConfigService.getInstance().getDefaultKey(\"" + id + "\"), null, null);}\n");

        fileHolder.line("public static " + fullClass + " " + funcName + "(DFKey key){return "
                + getClassName() + "." + funcName + "(key, null, null);}\n");

        fileHolder.line("public static " + fullClass + " " + funcName + "(Map<String, Object> customVars){return "
                + getClassName() + "." + funcName + "(ConfigService.getInstance().getDefaultKey(\"" + id + "\"), customVars, null);}\n");

        fileHolder.line("public static " + fullClass + " " + funcName + "(List<Object> vars){return "
                + getClassName() + "." + funcName + "(ConfigService.getInstance().getDefaultKey(\"" + id + "\"), null, vars);}\n");
    }

    protected SourceFile generateLHeader() {
        SourceFile sourceFile = new JavaSourceFile();
        Path path = basePath.resolve(getClassName() + ".java");
        sourceFile.setPath(path);
        sourceFile.line("package " + basePackage);
        lImports.forEach(i -> sourceFile.line(IMPORT + i));
        sourceFile.line("@Generated(\"Auto generated by TDM tool\")\npublic class " + getClassName() + " {\nstatic ObjectMapper mapper = new ObjectMapper();\nstatic {\n" +
                "    mapper = mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);\n" +
                "  }\n");

        //todo refactor
        // sourceFile.line("static {DataStore.getInstance().initFromEnvArgs();}");
        return sourceFile;
    }

    protected void generateLFooter(SourceFile fileHolder) {
        if (getClassName().equalsIgnoreCase("L")) {
            fileHolder.line("public static org.openqa.selenium.By get(String locator) {\n" +
                    "    String[] splitValues = locator.split(\"=\", 2);\n" +
                    "    String locType = splitValues[0];\n" +
                    "    String locValue = splitValues[1];\n" +
                    "\n" +
                    "    switch (locType) {\n" +
                    "      case \"id\":\n" +
                    "        return org.openqa.selenium.By.id(locValue);\n" +
                    "\n" +
                    "      case \"css\":\n" +
                    "        return org.openqa.selenium.By.cssSelector(locValue);\n" +
                    "\n" +
                    "      case \"xpath\":\n" +
                    "        return org.openqa.selenium.By.xpath(locValue);\n" +
                    "\n" +
                    "      case \"tag\":\n" +
                    "        return org.openqa.selenium.By.tagName(locValue);\n" +
                    "\n" +
                    "      default:\n" +
                    "        throw new RuntimeException(\"Not supported locator type\");\n" +
                    "    }\n" +
                    "  }");
        }
        fileHolder.line("}");
    }
}
