package by.issoft.tdm.data;

import by.issoft.tdm.data.dynamic.DynamicDataService;
import by.issoft.tdm.domain.data.DH;
import by.issoft.tdm.domain.enums.TDMMode;

/**
 * Created 3/21/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public class DataBuilder {

    String id;
    String name;
    //    Map<String, Object> customVars;
//    FakerMode mode;
//    DFKey key;
    String defaultValue;
    DataService dataService;
    DynamicDataService dynamicDataService;
    //List<Object> vars;
    DH holder;


    public DataBuilder(String id, String name, DataService dataService, DynamicDataService dynamicDataService) {
        this.id = id;
        this.name = name;
        this.dataService = dataService;
        this.dynamicDataService = dynamicDataService;
    }

    public DataBuilder with(DH holder) {
        this.holder = holder;
        return this;
    }

//    public DataBuilder with(DFKey key) {
//        this.key = key;
//        return this;
//    }
//
//    public DataBuilder with(FakerMode mode) {
//        this.mode = mode;
//        return this;
//    }
//
//    public DataBuilder with(Map<String, Object> customVars) {
//        this.customVars = customVars;
//        return this;
//    }
//
//    public DataBuilder with(String k, Object v) {
//        if (customVars == null) {
//            customVars = new HashMap<>();
//        }
//        customVars.put(k, v);
//        return this;
//    }
//
//    public DataBuilder with(List<Object> vars) {
//        this.vars = vars;
//        return this;
//    }

    public DataBuilder orElse(String defaultValue) {
        this.defaultValue = defaultValue;
        return this;
    }

    public String get() {//todo throw
        String data = dataService.getData(id, name, holder.getKey());
        if (data == null) {
            if (defaultValue == null) {
                throw new IllegalArgumentException("Null data //TODO");
            } else {
                return defaultValue;
            }
        }
        if (holder.getMode() == null || TDMMode.DF.equals(holder.getMode())) {
            //try to detect is this data is fakable?
            data = dynamicDataService.dynamizeData(id, data, holder);
        }
        return data;
    }
}
