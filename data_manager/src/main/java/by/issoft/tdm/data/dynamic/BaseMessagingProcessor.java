package by.issoft.tdm.data.dynamic;

import by.issoft.tdm.api.data.MessagingProcessor;
import by.issoft.tdm.config.ConfigService;
import by.issoft.tdm.domain.config.NGeneratorConfig;
import by.issoft.tdm.domain.config.TdmOptionParamsConst;
import by.issoft.tdm.domain.data.DH;
import by.issoft.tdm.domain.enums.GenType;
import by.issoft.tdm.generator.java.messages.TDM;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;

import java.util.Map;
import java.util.Set;

/**
 * Created 3/27/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
//TODO keep in mind about cache, possible bug
@Slf4j
public class BaseMessagingProcessor implements MessagingProcessor {

    NGeneratorConfig config;
    Class messagingClass;
    Object mObject;

    public BaseMessagingProcessor() {
        config = ConfigService.getInstance().getConfig().getGenConfig().values()
                .stream().filter(c -> GenType.MESSAGES.equals(c.getType()))
                .findFirst().orElse(null);
        if (config != null) {
            String pack = (String) config.getOptions().get(TdmOptionParamsConst.BASE_PACKAGE);

            Reflections reflections = new Reflections(pack);
            Set<Class<?>> c = reflections.getTypesAnnotatedWith(TDM.class);
            if (c.size() > 0) {
                messagingClass = (Class) c.toArray()[0];
                try {
                    mObject = messagingClass.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void prepare(String id, String data, DH holder, Map<String, Object> context) {
        //DFKey mKey = ConfigService.getInstance().getDefaultKey(config.getId());
        if (mObject != null) {
            context.put("m", mObject);
            try {
                context.put("l", messagingClass.getMethod("Loc").invoke(mObject));
            } catch (Exception ex) {
                //
                ex.printStackTrace();
            }
        }
    }
}
