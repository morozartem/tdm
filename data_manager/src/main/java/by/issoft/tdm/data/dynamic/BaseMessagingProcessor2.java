package by.issoft.tdm.data.dynamic;

import by.issoft.tdm.api.data.MessagingProcessor;
import by.issoft.tdm.config.ConfigService;
import by.issoft.tdm.domain.config.NGeneratorConfig;
import by.issoft.tdm.domain.config.TdmOptionParamsConst;
import by.issoft.tdm.domain.data.DFKey;
import by.issoft.tdm.domain.data.DH;
import by.issoft.tdm.domain.enums.GenType;
import by.issoft.tdm.generator.java.messages.MessageMethodEnum;
import by.issoft.tdm.generator.java.messages.TDM;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.CaseUtils;
import org.reflections.ReflectionUtils;
import org.reflections.Reflections;

import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created 3/27/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
//TODO keep in mind about cache, possible bug
@Slf4j
public class BaseMessagingProcessor2 implements MessagingProcessor {

    final static Pattern patternMessages = Pattern.compile("\\#\\{([\\w\\.]+)\\}", Pattern.MULTILINE);
    final static String MESSAGE_PREFIX = "#{";

    boolean inited;
    Map<MessageMethodEnum, Method> messagingMethods;
    NGeneratorConfig config;
    Class messagingClass;

    protected void init() {
        if (!inited) {
            config = ConfigService.getInstance().getConfig().getGenConfig().values()
                    .stream().filter(c -> GenType.MESSAGES.equals(c.getType()))
                    .findFirst().orElse(null);
            if (config != null) {
                String pack = (String) config.getOptions().get(TdmOptionParamsConst.BASE_PACKAGE);

                Reflections reflections = new Reflections(pack);
                Set<Class<?>> c = reflections.getTypesAnnotatedWith(TDM.class);
                if (c.size() > 0) {
                    messagingClass = (Class) c.toArray()[0];
                    //assume that we have only one resource

                    messagingMethods = new HashMap<>();

//                    Set<Method> methods = ReflectionUtils
//                            .getAllMethods(messagingClass, m -> m.getPgetParameterCount() == 2);

                    ReflectionUtils
                            .getAllMethods(messagingClass).stream().forEach(m -> {
                        int count = m.getParameterCount();
                        if (count == 0) {
                            messagingMethods.put(MessageMethodEnum.EMPTY, m);
                        } else if (count == 2) {
                            messagingMethods.put(MessageMethodEnum.FULL, m);
                        } else if (count == 1) {
                            if (m.getParameters()[0].getType().equals(DFKey.class)) {
                                messagingMethods.put(MessageMethodEnum.KEY, m);
                            } else {
                                messagingMethods.put(MessageMethodEnum.VARS, m);
                            }
                        }
                    });


//                    if (methods.size() == 1) {
//                        messagingMethod = (Method) methods.toArray()[0];
//                    }
                    inited = true;
                }
            }
        }
    }

    //TODO optimize this!!!!!
    protected DFKey getProperDFKey(String id, DFKey key) {
        return ConfigService.getInstance().getDefaultKey(id);
//        String lang = DFKeyHelper.detectLang(id, key);
//        DFKey mKey = DFKeyHelper.getKeyFromRunParams(config.getId());
//        if (mKey == null) {
//            if (config.getStructure().size() == 1) {
//                mKey = DFKey.build(lang);
//            }
//        } else {
//            DFKeyHelper.setLang(config.getId(), mKey, lang);
//        }
//        return mKey;
    }


    public String replaceWithMessages(String id, DFKey key, String data, Map<String, Object> customVars) {
        if (data.contains(MESSAGE_PREFIX)) {
            init();
            DFKey mKey = ConfigService.getInstance().getDefaultKey(config.getId());//getProperDFKey(id, key);
            if (mKey != null && !messagingMethods.isEmpty()) {
                try {

                    Matcher matcher = patternMessages.matcher(data);
                    Map<String, List<String>> placeholders = new HashMap<>();
                    while (matcher.find()) {
                        String replace = matcher.group(0);
                        List<String> parts = Arrays.asList(matcher.group(1).split("\\."));
                        placeholders.put(replace, parts);
                    }


                    // Object mObject = messagingClass.newInstance();
                    Map<String, String> replaceData = new HashMap<>();

                    Object messagesObj = null;
                    if (key != null) {
                        if (customVars != null && !customVars.isEmpty()) {
                            messagesObj = messagingMethods.get(MessageMethodEnum.FULL).invoke(null, customVars);
                        } else {
                            messagesObj = messagingMethods.get(MessageMethodEnum.KEY).invoke(null, customVars);
                        }
                    } else if (customVars != null && !customVars.isEmpty()) {
                        messagesObj = messagingMethods.get(MessageMethodEnum.VARS).invoke(null, customVars);
                    } else {
                        messagesObj = messagingMethods.get(MessageMethodEnum.EMPTY).invoke(null);
                    }

                    for (Map.Entry<String, List<String>> e : placeholders.entrySet()) {
                        try {
                            Object mObject2 = messagesObj;
                            for (String part : e.getValue()) {
                                Class rClass = mObject2.getClass();
                                String nextFunc = "get" + CaseUtils.toCamelCase(part, true);
                                Method mm = rClass.getMethod(nextFunc);
                                mObject2 = mm.invoke(mObject2);
                            }
                            replaceData.put(e.getKey(), (String) mObject2);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    for (Map.Entry<String, String> replace : replaceData.entrySet()) {
                        data = data.replace(replace.getKey(), replace.getValue());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return data;
    }

    @Override
    public void prepare(String id, String data, DH holder, Map<String, Object> context) {
        context.put("m", "messaging");
    }
}
