package by.issoft.tdm.data.fs;

import by.issoft.tdm.data.DataService;
import by.issoft.tdm.domain.config.NGeneratorConfig;
import by.issoft.tdm.domain.data.DFInfo;
import by.issoft.tdm.domain.data.DFKey;
import by.issoft.tdm.domain.data.MetaStore;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created 09.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Slf4j
public class FileSystemDataService extends DataService {

    public FileSystemDataService() {
        super();
        //if (!TDMMode.DF.equals(ConfigService.getInstance().getMode())) {
        // <editor-fold desc="append file listener section">
//            try {
//                String fsMode = config.getStructureType();
//                FileAlterationObserver observer = new FileAlterationObserver(config.getRealDataPath().toFile());
//                FileAlterationMonitor monitor = new FileAlterationMonitor(config.getFsListenerInterval() * 1000);
//
//                observer.addListener(new FileAlterationListenerAdaptor() {
//
//                    Path root = config.getRealDataPath();
//
//                    @Override
//                    public void onDirectoryCreate(File directory) {
//                        handleDirChange(directory, ChangeType.CREATE);
//                    }
//
//                    @Override
//                    public void onDirectoryChange(File directory) {
//                        handleDirChange(directory, ChangeType.CHANGE);
//                    }
//
//                    @Override
//                    public void onDirectoryDelete(File directory) {
//                        handleDirChange(directory, ChangeType.DELETE);
//                    }
//
//                    @Override
//                    public void onFileCreate(File file) {
//                        handleFileChange(DFInfo.build(file.toPath(), root, fsMode), ChangeType.CREATE);
//                    }
//
//                    @Override
//                    public void onFileChange(File file) {
//                        handleFileChange(DFInfo.build(file.toPath(), root, fsMode), ChangeType.CHANGE);
//                    }
//
//                    @Override
//                    public void onFileDelete(File file) {
//                        handleFileChange(DFInfo.build(file.toPath(), root, fsMode), ChangeType.DELETE);
//                    }
//
//                });
//                monitor.addObserver(observer);
//
//                monitor.start();
//            } catch (Exception e) {
//                e.printStackTrace();
//                log.error(e.getMessage(), e);
//            }
        // </editor-fold>
        //}
    }

    @Override
    public void readDataStructure() {
        metaStore.clear();
        entryStore.clear();
        config.getGenConfig().entrySet().stream().forEach(e -> {
            try {
                NGeneratorConfig generatorConfig = e.getValue();
                String fsMode = generatorConfig.getStructureType();
                Path root = generatorConfig.getRealDataPath();
                MetaStore ms = new MetaStore();
                Files.walk(generatorConfig.getRealDataPath())
                        .filter(f -> Files.isRegularFile(f) && f.getFileName().toString().endsWith("json"))
                        .forEach(p -> {
                            DFInfo info = DFInfo.build(p, root, fsMode);
                            ms.put(info);
                            log.debug("Process file [" + info.getName() + "] " + info.getFile().getAbsolutePath());
                        });
                metaStore.put(e.getKey(), ms);
            } catch (IOException ex) {
                //TODO
                ex.printStackTrace();
                log.error(ex.getMessage(), ex);
            }
        });

    }

    @Override
    public String loadData(String id, String name, DFKey dfKey) {
        File file = getDataFile(id, name, dfKey);
        if (file != null) {
            try {
                return new String(Files.readAllBytes(file.toPath()), StandardCharsets.UTF_8);
            } catch (IOException e) {
                e.printStackTrace();
                log.error(e.getMessage(), e);
            }
        }//todo
        return null;
    }

    @Override
    public DFInfo storeData(String id, String name, DFKey dfKey, String data) {
        Path filePath = getFile(id, name, dfKey);
        File file = filePath.toFile();
        file.getParentFile().mkdirs();
        //write data
        try {
            Files.write(filePath, data.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            //todo rollback all ? and throw proper ex
            return null;
        }
        return new DFInfo(file, name, dfKey);
    }

    @Override
    public boolean removeData(String id, String name, DFKey dfKey) {
        Path filePath = getFile(id, name, dfKey);
        File file = filePath.toFile();
        try {
            FileUtils.forceDelete(file);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private Path getFile(String id, String name, DFKey dfKey) {
        //generate proper path file
        NGeneratorConfig generatorConfig = config.getGenConfig().get(id);
        String fsMode = generatorConfig.getStructureType();//TODO
        Path filePath = generatorConfig.getRealDataPath();

        if (dfKey != null) {
            for (String subPath : dfKey.getHeritageKey()) {
                filePath = filePath.resolve(subPath);
            }
        }
        filePath = filePath.resolve(name + ".json");
        return filePath;
    }

}
