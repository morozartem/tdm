package by.issoft.tdm.data.dynamic;

import by.issoft.tdm.api.data.DataProcessor;
import by.issoft.tdm.api.data.MessagingProcessor;
import by.issoft.tdm.domain.common.Pair;
import by.issoft.tdm.domain.data.DFKey;
import by.issoft.tdm.domain.data.DH;
import by.issoft.tdm.domain.enums.FakerMode;
import lombok.extern.slf4j.Slf4j;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.RuntimeConstants;

import java.io.StringWriter;
import java.util.*;

/**
 * Created 3/27/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Slf4j
public class DynamicDataService {

    //########### cache section
    //data = faked data
    protected Map<String, String> singletonCacheMap;
    //gen id/threadId = data
    protected Map<Pair<String, Long>, String> threadCacheMap;

    protected Map<String, DFKey> messagesKeysMap;

    //############### end

    List<DataProcessor> dataProcessorList;
    String messagesId;

    public DynamicDataService() {
        this.singletonCacheMap = new WeakHashMap<>();
        this.threadCacheMap = new WeakHashMap<>();
        this.messagesKeysMap = new WeakHashMap<>();

        dataProcessorList = Arrays.asList(new JavaFakerProcessor(), new BaseMessagingProcessor());
//        NGeneratorConfig config = ConfigService.getInstance().getConfig().getGenConfig().values()
//                .stream().filter(c -> GenType.MESSAGES.equals(c.getType()))
//                .findFirst().orElse(null);
//        if (config != null) {
//            messagesId = config.getId();
//        }

        Velocity.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, "org.apache.velocity.runtime.log.NullLogChute");
    }

    public static void main(String[] args) {
        String data = "hy my name is ${f.name().firstName()} and message is ${m} with %s \n and ${num} + ${bool} with $str";
        Map<String, Object> varrs = new HashMap<>();
        varrs.put("num", 1);
        varrs.put("bool", true);
        varrs.put("str", "oooppppaaaa");
        DH dh = new DH().with(DFKey.build("en")).with(Arrays.asList("testMode")).with(varrs);
        System.out.println(new DynamicDataService().dynamizeData("1", data, dh));
    }

    public String dynamizeData(String id, String data, DH holder) {
        FakerMode mode = holder.getMode();
        if (mode == null) {
            mode = FakerMode.PROXY;
        }
        String result = null;
        switch (mode) {
            case PROXY: {
                result = processData(id, data, holder);
                break;
            }
            case SINGLETON: {
                result = singletonCacheMap.get(data);
                if (result == null) {
                    result = processData(id, data, holder);
                    singletonCacheMap.put(data, result);
                }
                break;
            }
            case THREAD: {
                Pair k = new Pair<>(id, Thread.currentThread().getId());
                result = threadCacheMap.get(k);
                if (result == null) {
                    result = processData(id, data, holder);
                    threadCacheMap.put(k, result);
                }
                break;
            }
        }
        return result;
    }

    protected String processData(String id, String data, DH holder) {

        if (data.contains("%") && holder.getVars() != null) {
            data = String.format(data, holder.getVars().toArray());
        }

        if (data.contains("$")) {
            Map<String, Object> context = new HashMap<>();
            if (holder.getCustomVars() != null) {
                holder.getCustomVars().entrySet().forEach(e -> context.put(e.getKey(), e.getValue()));
            }

            for (DataProcessor dp : dataProcessorList) {

                dp.prepare(id, data, holder, context);

                if (dp instanceof MessagingProcessor && !id.equalsIgnoreCase(messagesId)) {

                }
            }
            StringWriter sw = new StringWriter();
            Velocity.evaluate(new VelocityContext(context), sw, "log", data);

            data = sw.toString();
        }

//        if (!id.equalsIgnoreCase(messagesId)) {
//            //process Messaging
//            data = messagingService.replaceWithMessages(id, key, data, customVars);
//        }
//        data = customFunctionsService.process(id, key, data, customVars, mode);
//        //invoke Faker
//        data = fakerService.fakerize(id, key, data, customVars, mode);
        return data;

    }
}
