package by.issoft.tdm.data;

import by.issoft.tdm.api.DataChangeListener;
import by.issoft.tdm.config.ConfigService;
import by.issoft.tdm.domain.config.NGeneratorConfig;
import by.issoft.tdm.domain.config.TDMConfig;
import by.issoft.tdm.domain.data.DFInfo;
import by.issoft.tdm.domain.data.DFKey;
import by.issoft.tdm.domain.data.EntryStore;
import by.issoft.tdm.domain.data.MetaStore;
import by.issoft.tdm.domain.enums.ChangeType;
import by.issoft.tdm.domain.enums.TDMMode;
import by.issoft.tdm.domain.generator.GenerationResults;
import by.issoft.tdm.generator.BaseGenerator;
import by.issoft.tdm.utils.SerializeFileUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created 09.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Slf4j
public abstract class DataService {

    protected List<DataChangeListener> listeners;
    @Getter
    protected Map<String, MetaStore> metaStore;
    @Getter
    protected Map<String, EntryStore> entryStore;
    protected TDMConfig config;
    //protected Map<String, Path> rootsMap;
    String INFO_FILENAME = "tdm.info";

    public DataService() {
        metaStore = new HashMap<>();
        entryStore = new HashMap<>();
        config = ConfigService.getInstance().getConfig();
        //  rootsMap = new HashMap<>();
    }

    public void initDataService() throws IOException {
        log.info("############### {} mode [{}] ##############", this.getClass().getCanonicalName(), ConfigService.getInstance().getMode());
        //read data files
        readDataStructure();
        //generate meta info object
        if (!ConfigService.getInstance().getMode().equals(TDMMode.DF)) {

            GenerationResults oldResults = null;
            File infoFile = Paths.get(INFO_FILENAME).toFile();
            try {
                oldResults = new SerializeFileUtils<GenerationResults>().deserialize(infoFile);
            } catch (Exception ex) {
                //log.error(ex.getMessage(), ex);
                oldResults = new GenerationResults();
            }

            GenerationResults newResults = new GenerationResults();

            for (Map.Entry<String, NGeneratorConfig> e : config.getGenConfig().entrySet()) {
                NGeneratorConfig genConfig = e.getValue();
                MetaStore ms = metaStore.get(e.getKey());
                BaseGenerator baseGenerator = new BaseGenerator();
                try {
                    baseGenerator.regenerate(e.getKey(), ms, genConfig, config, oldResults, newResults);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            try {
                new SerializeFileUtils<GenerationResults>().serialize(infoFile, newResults);
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
    }

    protected abstract void readDataStructure() throws IOException;

    public abstract String loadData(String id, String name, DFKey dfKey);

    public abstract DFInfo storeData(String id, String name, DFKey dfKey, String Data);

    public abstract boolean removeData(String id, String name, DFKey dfKey);

    public String getData(String id, String name) {
        return getData(id, name, null);
    }

    private EntryStore getEntryStoreForId(String id) {
        EntryStore e = entryStore.get(id);
        if (e == null) {
            e = new EntryStore();
            entryStore.put(id, e);
        }
        return e;
    }

    public String getData(String id, String name, DFKey dfKey) {
        EntryStore e = getEntryStoreForId(id);
        if (dfKey == null) {
            //try to get default key
            dfKey = ConfigService.getInstance().getDefaultKey(id);
        }
        String data = getDataFromEntryStore(id, name, dfKey, e);
//                e.getData(name, dfKey);
//        if (data == null) {
//            data = loadData(id, name, dfKey);
//            if (data != null) {
//                e.put(name, dfKey, data);
//            } else if (dfKey != null) {
//                //try to find any default value
//                return getData(id, name);
//            }
//        }
        return data;
    }


    private String getDataFromEntryStore(String id, String name, DFKey dfKey, EntryStore e) {
        String data = e.getData(name, dfKey);
        if (data == null) {
            data = loadData(id, name, dfKey);
            if (data != null) {
                e.put(name, dfKey, data);
            } else if (dfKey != null) {
                //try to find any default value
                return getDataFromEntryStore(id, name, null, e);
            }
        }
        return data;
    }

    public File getDataFile(String id, String name, DFKey dfKey) {
        if (metaStore.containsKey(id)) {
            return metaStore.get(id).getDataFile(name, dfKey);
        }
        return null;
    }

    public void setData(String id, String name, DFKey dfKey, String data) {
        //save data to file/...
        DFInfo dfInfo = storeData(id, name, dfKey, data);
        if (dfInfo.getFile() != null) {
            //update meta
            metaStore.get(id).put(dfInfo);
            //update entry store
            getEntryStoreForId(id).put(name, dfKey, data);
            //regenerate if needed??
            //TODO regenerate!?
        }
    }

    public void delData(String id, String name, DFKey dfKey) {
        if (removeData(id, name, dfKey)) {
            //update meta
            metaStore.get(id).remove(name, dfKey);
            //update entry store
            getEntryStoreForId(id).remove(name, dfKey);
            //regenerate if needed??
            //TODO regenerate!?
        }
    }

    // <editor-fold desc="listeners" collapsed="true>
    //TODO update!!!!
    public void addDataChangeListener(DataChangeListener listener) {
        if (listeners == null) {
            listeners = new ArrayList<>();
        }
        listeners.add(listener);
    }

    protected void handleFileChange(DFInfo info, ChangeType changeType) {
        try {
            switch (changeType) {
                case CHANGE:
                case CREATE: {
                    //delete data and meta, and reload file
                    entryStore.remove(info.getName());
                    //TODO rebuild all for name
                    break;
                }
                case DELETE: {
                    //delete data and meta
                    entryStore.remove(info.getName(), info.getKey());
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        listeners.stream().forEach(l -> l.fileChange(info, changeType));
    }

    protected void handleDirChange(File file, ChangeType changeType) {
        //TODO seems that need to regenerate everything
        listeners.stream().forEach(l -> l.dirChange(file.toPath(), changeType));

    }
    // </editor-fold>


}
