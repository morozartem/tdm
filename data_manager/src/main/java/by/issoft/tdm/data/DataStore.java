package by.issoft.tdm.data;


import by.issoft.tdm.data.dynamic.DynamicDataService;
import by.issoft.tdm.data.fs.FileSystemDataService;
import by.issoft.tdm.domain.data.DFKey;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * Created 16.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Slf4j
public class DataStore {

    private static DataStore instance = new DataStore();
    @Getter
    protected DataService dataService;
    @Getter
    protected DynamicDataService dynamicDataService;

    private DataStore() {
        log.info("************** Starting DataStore *****************");
        dataService = new FileSystemDataService();
        try {
            dataService.initDataService();
            dynamicDataService = new DynamicDataService();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static DataStore getInstance() {
        return instance;
    }

    public DataBuilder with(String id, String name) {
        return new DataBuilder(id, name, dataService, dynamicDataService);
    }

    public void store(String id, String name, DFKey key, String data) {
        dataService.setData(id, name, key, data);
    }

    public void remove(String id, String name, DFKey key) {
        dataService.delData(id, name, key);
    }


}
