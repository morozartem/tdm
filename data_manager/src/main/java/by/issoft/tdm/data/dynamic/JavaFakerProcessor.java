package by.issoft.tdm.data.dynamic;

import by.issoft.tdm.api.data.DataProcessor;
import by.issoft.tdm.domain.data.DH;
import by.issoft.tdm.utils.DFKeyHelper;
import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created 10.02.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Slf4j
public class JavaFakerProcessor implements DataProcessor {

    Map<String, Faker> fakerMap = new HashMap<>();

    protected Faker initFakerWithLocale(String lang) {
        Faker faker = fakerMap.get(lang);
        if (faker == null) {
            faker = new Faker(new Locale(lang));
            fakerMap.put(lang, faker);
        }
        return faker;
    }


    @Override
    public void prepare(String id, String data, DH holder, Map<String, Object> context) {
        Faker f = initFakerWithLocale(DFKeyHelper.detectLang(id, holder.getKey()));
        context.put("f", f);
    }
}
