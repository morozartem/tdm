package by.issoft.tdm.utils;


import by.issoft.tdm.domain.enums.SchemaType;
import by.issoft.tdm.domain.schema.SchemaItem;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Created 06.02.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
//TODO!!!!!!!!!!!! NULL NODES!!!!!
public class SchemaExtractor {

    static char[] fileSeparators = {'_', '-'};
    List<String> ignoreFieldPrefixes = Arrays.asList("_", "$");

    public static void main(String[] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(new File("C:\\Projects\\job\\orderboard\\horizonautomation\\src\\test\\resources\\data\\jobRole.json"));
        SchemaExtractor extractor = new SchemaExtractor();
        SchemaItem item = extractor.getSchemaForNodes(Arrays.asList(node), "root");
        System.out.println(item);
    }

    public SchemaItem getSchemaForNodes(List<JsonNode> nodes, String rootName) {

        String name = WordUtils.capitalizeFully(rootName, fileSeparators);
        for (char separator : fileSeparators) {
            name = StringUtils.remove(name, separator);
        }
        //calculate schema
        JsonNode mainNode = nodes.get(0);
        boolean isArray = false;
        if (mainNode.isArray()) {//todo this is possible a bug, cause first can be array, but thr rest are not?!
            List<JsonNode> innerNodes = new ArrayList<>();
            for (JsonNode node : nodes) {
                node.elements().forEachRemaining(innerNodes::add);
            }
            nodes = innerNodes;
            mainNode = nodes.get(0);
            isArray = true;
        }
        if (nodes.size() > 1) {
            for (JsonNode node : nodes) {
                mainNode = JsonMerge.merge(mainNode, node);
            }
        }

        ObjectNode objectNode = new SchemaGenerator().schemaFromExample(mainNode);

        SchemaItem root = build(name, objectNode);
        //govnokod
        if (isArray) {
            root.setArrayItemType(root.getType());
            root.setType(SchemaType.ARRAY);
        }
        root.setDataName(rootName);
        return root;
    }

    protected SchemaItem build(String name, JsonNode node) {
        if (ignoreFieldPrefixes.contains(name.substring(0, 1))) {
            System.out.println("Ignore " + name);
            return null;
        } else {
            SchemaItem item = new SchemaItem();
            item.setType(detectType(node.findValue("type").asText()));
            item.setName(name);

            if (item.getType().equals(SchemaType.ARRAY)) {
                SchemaType arrayType = detectType(node.get("items").findValue("type").asText());
                if (arrayType.equals(SchemaType.OBJECT)) {
                    SchemaItem childArrayItem = build("items", node.get("items"));
                    item.setItemList(childArrayItem.getItemList());
                    item.setArrayItemType(SchemaType.OBJECT);
                } else {
                    item.setArrayItemType(arrayType);
                }
            } else {
                JsonNode properties = node.findValue("properties");
                if (properties != null) {
                    //has child nodes
                    Map<String, JsonNode> childs = StreamSupport.stream(
                            Spliterators.spliteratorUnknownSize(properties.fields(), Spliterator.ORDERED)
                            , false).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));


                    List<SchemaItem> items = childs.entrySet().stream()
                            .filter(e -> !ignoreFieldPrefixes.contains(e.getKey().substring(0, 1)))
                            .map(c -> {
                                String itemName = c.getKey();
                                SchemaItem childSchema = build(itemName, c.getValue());
                                JsonNode locatorNode = childs.get("_" + itemName);
                                if (locatorNode != null) {
                                    childSchema.addProperty("locator", locatorNode.get("val").asText());
                                }
                                childSchema.setParent(item);
                                return childSchema;
                            }).collect(Collectors.toList());

                    item.setItemList(items);
                }
            }
            return item;
        }
    }

    protected SchemaType detectType(String type) {
        switch (type) {

            case "number":
                return SchemaType.DECIMAL;
            case "string":
                return SchemaType.STRING;
            case "boolean":
                return SchemaType.BOOLEAN;
            case "integer":
                return SchemaType.INTEGER;
            case "array":
                return SchemaType.ARRAY;
            default:
                return SchemaType.OBJECT;
        }
    }
}
