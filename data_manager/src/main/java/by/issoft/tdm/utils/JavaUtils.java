package by.issoft.tdm.utils;


import by.issoft.tdm.domain.enums.SchemaType;

/**
 * Created 12.02.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public class JavaUtils {

    public static String getCompatibleType(SchemaType type, boolean isSimple) {
        switch (type) {
            case STRING:
                return "String";
            case BOOLEAN:
                return isSimple ? "boolean" : "Boolean";
            case DECIMAL:
                return isSimple ? "long" : "Long";
            case INTEGER:
                return isSimple ? "int" : "Integer";
        }
        return "Unknown";
    }
}
