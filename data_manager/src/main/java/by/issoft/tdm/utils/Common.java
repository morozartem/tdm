package by.issoft.tdm.utils;

import by.issoft.tdm.domain.data.DFKey;
import com.github.javafaker.Faker;

/**
 * Created 16.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public class Common {

    public static Integer atoi(String s) {
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            return 0;
        }
    }

    public static DFKey fk(DFKey... keys) {
        return (keys == null || keys.length == 0) ? null : keys[0];
    }


    public static void main(String[] args) {
        Faker f = new Faker();
        System.out.println(f.random().nextInt(0, 1005));
    }
}
