package by.issoft.tdm.utils;

import java.io.*;

/**
 * Created 18.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public class SerializeFileUtils<T> {

    public void serialize(File f, T o) throws IOException {
        try (FileOutputStream fileOutputStream
                     = new FileOutputStream(f)) {
            ObjectOutputStream objectOutputStream
                    = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(o);
            objectOutputStream.flush();
            objectOutputStream.close();
        }
    }

    public T deserialize(File f) throws IOException, ClassNotFoundException {
        try (FileInputStream fileInputStream
                     = new FileInputStream(f)) {
            ObjectInputStream objectInputStream
                    = new ObjectInputStream(fileInputStream);
            T res = (T) objectInputStream.readObject();
            objectInputStream.close();
            return res;
        }
    }
}
