package by.issoft.tdm.utils;

import by.issoft.tdm.config.ConfigService;
import by.issoft.tdm.domain.data.DFKey;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created 18.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public class DFKeyHelper {

    static Map<String, List<String>> genStructures = new HashMap<>();
    static Map<String, Integer> langKeys = new HashMap<>();
    static String defaultLang;


    protected static Integer detectLangIndex(String id, DFKey key) {
        Integer keyIndex = langKeys.get(id);
        if (keyIndex == null && key != null) {
            List<String> structure = genStructures.get(id);
            if (structure == null) {
                try {
                    structure = ConfigService.getInstance().getConfig().getGenConfig().get(id).getStructure();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    //TODO throw
                }
            }
            if (structure != null && key.getHeritageKey() != null) {
                //we need to detect proper lang
                for (int i = 0; i < structure.size(); i++) {
                    String k = structure.get(i);
                    if (k.contains("locale") || k.contains("lang")) {
                        //store this index, for futher fakerization
                        keyIndex = i;
                        langKeys.put(id, keyIndex);
                        break;
                    }
                }
            }
        }
        return keyIndex;
    }

    public static String detectLang(String id, DFKey key) {
        Integer keyIndex = detectLangIndex(id, key);
        if (keyIndex != null) {
            return key.getHeritageKey().get(keyIndex);
        } else {
            return getDefaultLang();
        }
    }

    //    public static DFKey setLang(String id, DFKey key, String lang) {
//        Integer keyIndex = detectLangIndex(id, key);
//        if (keyIndex != null) {
//            key.getHeritageKey().set(keyIndex, lang);
//        }
//        return key;
//    }
//
    public static String getDefaultLang() {
        if (defaultLang == null) {
            defaultLang = ConfigService.getInstance().getConfig().getDefaultLocale();
        }
        return defaultLang;
    }
//
//    public static DFKey getKeyFromRunParams(String id) {
//        return ConfigService.getInstance().getDefaultKey(id);
//    }
}
