package by.issoft.tdm.config;


import by.issoft.tdm.domain.config.NGeneratorConfig;
import by.issoft.tdm.domain.config.TDMConfig;
import by.issoft.tdm.domain.data.DFKey;
import by.issoft.tdm.domain.enums.TDMMode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


/**
 * Created 09.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Slf4j
public class ConfigService {

    protected static ConfigService instance;

    static ObjectMapper mapper = new ObjectMapper();

    private static String MODE = "mode";
    private static String CONFIG_FILE = "configFile";
    //private static String LEVEL_PREFIX = "LP";

    //String levelPrefix;
    String configFileName;
    @Getter
    TDMMode mode;
    @Getter
    Map<String, DFKey> defaultKeys;
    @Getter
    TDMConfig config;

    String defaultLang = "en";

    Map<String, String> envs = System.getenv();
    Properties properties = System.getProperties();


    public ConfigService() {
        this.defaultKeys = new HashMap<>();
        init();

    }

    public static ConfigService getInstance() {
        if (instance == null) {
            synchronized (ConfigService.class) {
                if (instance == null) {
                    instance = new ConfigService();
                }
            }
        }
        return instance;
    }

    public static void main(String[] args) {
        System.out.println(ConfigService.getInstance().getDefaultKeys());
    }

    public DFKey getDefaultKey(String id) {
        return defaultKeys.get(id);
    }

    protected void init() {

        //find and read config

        readGeneralConfig();
        Path projectPath = Paths.get("");
        try {
            Path configPath = Files.walk(projectPath)
                    .filter(f -> f.getFileName().toString().contains(configFileName))
                    .findFirst().orElse(null);
            if (configPath != null) {
                config = mapper.readValue(configPath.toFile(), TDMConfig.class);
                config.setConfigFile(configPath);
                //process it
                config.getGenConfig().entrySet().forEach(e -> {
                    NGeneratorConfig gen = e.getValue();
                    gen.setId(e.getKey());
                    try {
                        gen.setRealDataPath(projectPath.resolve(gen.getDataPath()));
                        gen.setRealGeneratePath(projectPath.resolve(gen.getGeneratePath()));
                    } catch (Exception ex) {
                        //there no need to handle that, cause we can have config without generators
                    }
                });
            } else {
                throw new IllegalArgumentException("Unable to read config in this folder " + projectPath.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Unable to build config. " + e.getMessage(), e);
            throw new RuntimeException("Unable to build config. " + e.getMessage(), e);
        }

        //read env/cli vars and cache default key
        readRuntimeProps();
    }

    private void readGeneralConfig() {
        //read mode
        mode = TDMMode.valueOf(getExternalProperty(MODE, "df").toUpperCase());
        configFileName = getExternalProperty(CONFIG_FILE, "tdm.json");
        //levelPrefix = getExternalProperty(LEVEL_PREFIX, "ll");
    }

    private String getExternalProperty(String name, String defValue) {
        String res = envs.get(name);
        if (res == null) {
            res = properties.getProperty(name);
        }
        if (res == null && defValue != null) {
            res = defValue;
        }
        if (name.equalsIgnoreCase("lang") && res == null) {
            res = defaultLang;
        }
        return res;
    }

    private void readRuntimeProps() {

        Set<String> structureNames = new HashSet<>();

        config.getGenConfig().values().forEach(gen -> {
            if (!gen.getStructure().isEmpty()) {
                structureNames.addAll(gen.getStructure());
            }
        });
        Map<String, String> structureValues = new HashMap<>();
        structureNames.forEach(s -> structureValues.put(s, getExternalProperty(s, null)));

        config.getGenConfig().entrySet().forEach(gen -> {
            if (!gen.getValue().getStructure().isEmpty()) {
                DFKey dfKey = new DFKey();
                gen.getValue().getStructure().forEach(s -> {
                    dfKey.add(structureValues.get(s));
                });
                defaultKeys.put(gen.getKey(), dfKey);
            }
        });
    }
}
