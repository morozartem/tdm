package by.issoft.tdm.git;


import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ProgressMonitor;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.io.File;
import java.io.IOException;

/**
 * Created 3/22/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
@Deprecated()
public class GitTest {


    static final File localPath = new File("C:\\Projects\\job\\tdm\\gitresult\\");
    static final String USERNAME = "morozartem";
    static final String PASS = "ghjcnjq_1";
    private static final String REMOTE_URL = "https://morozartem:ghjcnjq_1@bitbucket.org/morozartem/tdm_manual_data.git";

    public static void main(String[] args) throws IOException, GitAPIException {
//        FileRepositoryBuilder builder = new FileRepositoryBuilder();
//        try (Repository repository = builder.setGitDir(localPath)
//                .readEnvironment() // scan environment GIT_* variables
//                .findGitDir() // scan up the file system tree
//                .build()) {
//            System.out.println("Having repository: " + repository.getDirectory());
//
//            // the Ref holds an ObjectId for any type of object (tree, commit, blob, tree)
//            Ref head = repository.exactRef("refs/heads/master");
//            System.out.println("Ref of refs/heads/master: " + head);
        String fname = "FFF" + String.valueOf(Math.random()).substring(2);
        try (Git git = Git.open(localPath)) {

//                FetchResult result = git.fetch().setCheckFetchedObjects(true).call();
//                System.out.println("Messages: " + result.getMessages());

            git.pull().call();

            if (false) {
                File myFile = new File(localPath, fname);
                try {
                    myFile.createNewFile();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                git.add()
                        .addFilepattern(".")
                        .call();

                // and then commit the changes
                git.commit()
                        .setMessage("Added " + fname)
                        .call();

                PushCommand pushCommand = git.push();
                pushCommand.setCredentialsProvider(new UsernamePasswordCredentialsProvider(USERNAME, PASS));
                pushCommand.call();
            }

        }

        //}

    }

    public static void main3(String[] args) throws IOException, GitAPIException {

//        if(localPath.exists()) {
//            FileUtils.deleteDirectory(localPath);
//        }

        // then clone
        System.out.println("Cloning from " + REMOTE_URL + " to " + localPath);
        try (Git result = Git.cloneRepository()
                .setURI(REMOTE_URL)

                .setDirectory(localPath)
                .setProgressMonitor(new SimpleProgressMonitor())
                .call()) {
            // Note: the call() returns an opened repository already which needs to be closed to avoid file handle leaks!
            System.out.println("Having repository: " + result.getRepository().getDirectory());
        }

        // clean up here to not keep using more and more disk-space for these samples
        // FileUtils.deleteDirectory(localPath);
    }

    private static class SimpleProgressMonitor implements ProgressMonitor {
        @Override
        public void start(int totalTasks) {
            System.out.println("Starting work on " + totalTasks + " tasks");
        }

        @Override
        public void beginTask(String title, int totalWork) {
            System.out.println("Start " + title + ": " + totalWork);
        }

        @Override
        public void update(int completed) {
            System.out.print(completed + "-");
        }

        @Override
        public void endTask() {
            System.out.println("Done");
        }

        @Override
        public boolean isCancelled() {
            return false;
        }
    }
}
