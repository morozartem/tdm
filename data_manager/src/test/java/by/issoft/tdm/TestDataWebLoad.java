package by.issoft.tdm;

import by.issoft.tdm.data.DataStore;
import by.issoft.tdm.domain.data.DFKey;
import by.issoft.tdm.domain.data.MetaStore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created 3/23/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public class TestDataWebLoad {

    public static void main(String[] args) {
        System.setProperty("configFile", "tdm1.json");
        String ID = "mdata";
        Map<String, MetaStore> storeMap = DataStore.getInstance().getDataService().getMetaStore();
        MetaStore store = storeMap.get(ID);
        List<ManualDataHolder> mdh = store.getMetaMap().entrySet().stream().flatMap(e ->
                e.getValue().keySet().stream().map(k ->
                        new ManualDataHolder(k, e.getKey())
                )
        ).collect(Collectors.toList());
        String path = "path1";
        //String path = null;

//        mdh.stream().filter(m -> path==null || path.equalsIgnoreCase(m.getPath()))
//                .forEach(m -> System.out.println(m));
        System.out.println("1111");
        DataStore.getInstance().store(ID, "sample6", DFKey.build("path1", "roman"), "{ \"temp\":2}");
        mdh = DataStore.getInstance().getDataService().getMetaStore().get(ID).getMetaMap().entrySet().stream().flatMap(e ->
                e.getValue().keySet().stream().map(k ->
                        new ManualDataHolder(k, e.getKey())
                )
        ).collect(Collectors.toList());
        mdh.stream().filter(m -> path == null || path.equalsIgnoreCase(m.getPath()))
                .sorted(Comparator.comparing(ManualDataHolder::getUser))
                .forEach(m -> System.out.println(m));
        //System.out.println(DataStore.getInstance().with("mdata", "first").with(DFKey.build("path1", "amoroz")).get());
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ManualDataHolder {

        DFKey key;
        String name;

        public String getPath() {
            return key.first();
        }

        public String getUser() {
            return key.second();
        }


    }
}
