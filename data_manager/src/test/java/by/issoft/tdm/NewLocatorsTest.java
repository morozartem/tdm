package by.issoft.tdm;

import by.issoft.tdm.config.ConfigService;
import by.issoft.tdm.domain.config.NGeneratorConfig;
import by.issoft.tdm.domain.generator.GenFileResult;
import by.issoft.tdm.domain.schema.SchemaItem;
import by.issoft.tdm.generator.java.locators.LocatorJavaGenerator;
import by.issoft.tdm.utils.SchemaExtractor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created 3/26/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public class NewLocatorsTest {

    public static void main(String[] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        File f = new File("C:\\Projects\\job\\joint\\ixo\\src\\test\\resources\\newres\\locators\\en\\locators.json");
        JsonNode node = mapper.readTree(f);
        SchemaItem item = new SchemaExtractor().getSchemaForNodes(Arrays.asList(node), "Locators");
        LocatorJavaGenerator generator = new LocatorJavaGenerator();
        NGeneratorConfig generatorConfig = ConfigService.getInstance().getConfig().getGenConfig().get("data");
        generator.init(generatorConfig);
        Map<String, GenFileResult> results = new HashMap<>();
        generator.generateObject(item, results);
        System.out.println("1");
        //DataStore.getInstance().with("locators", "")
    }
}
