package by.issoft.tdm;

import org.reflections.ReflectionUtils;
import org.reflections.Reflections;

import java.lang.reflect.Method;
import java.util.Set;

/**
 * Created 3/26/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public class TestMClassFind {

    public static void main(String[] args) {
        Reflections reflections = new Reflections("by.issoft");
        Set<Class<?>> c = reflections.getTypesAnnotatedWith(Deprecated.class);
        if (c.size() > 0) {
            Class cc = (Class) c.toArray()[0];
            Set<Method> methods = ReflectionUtils.getAllMethods(cc, m -> m.getName().equals("messages"));

        }
    }
}
