package by.issoft.tdm;

import by.issoft.tdm.data.DataStore;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created 16.03.2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public class GlobalTest {

    public static void main(String[] args) throws IOException {
        if (true) {
            String basePath = System.getProperty("user.dir");
            FileUtils.deleteDirectory(new File(basePath + "\\gen_results\\gen"));
            new File(basePath + "\\tdm.info").delete();
        }
        System.out.println(DataStore.getInstance().with("data", "file1").get());
    }
}
