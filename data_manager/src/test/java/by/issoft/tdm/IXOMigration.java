package by.issoft.tdm;

import pl.jalokim.propertiestojson.util.PropertiesToJsonConverter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created 3/25/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public class IXOMigration {

    public static void migrateLocators() throws IOException {
        Path p = Paths.get("C:\\Projects\\job\\joint\\ixo\\src\\test\\resources\\lang\\");
        Path root = Paths.get("C:\\Projects\\job\\joint\\ixo\\src\\test\\resources\\newres\\locators\\");
        Files.walk(p).filter(f -> f.toString().contains("locators")).forEach(f -> {
            File file = f.toFile();
            System.out.println(file.getName());
            Properties properties = new Properties();
            try {
                properties.load(new FileInputStream(file));
                //properties.keySet().stream().forEach(k-> System.out.println(k));
                String json = new PropertiesToJsonConverter().convertToJson(properties);
                //System.out.println(json);
                json = removeUTFCharacters(json).toString();
                String lang = file.getName().replace("locators-", "").replace(".properties", "").substring(0, 2).replace("sw", "sv");
                Path newFile = root.resolve(lang).resolve("locators.json");
                File file2 = newFile.toFile();
                file2.getParentFile().mkdirs();
                //write data
                try {
                    Files.write(newFile, json.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public static void migrateMesasges() throws IOException {
        Path p = Paths.get("C:\\Projects\\job\\joint\\ixo\\src\\test\\resources\\lang\\");
        Path root = Paths.get("C:\\Projects\\job\\joint\\ixo\\src\\test\\resources\\newres\\messages\\");
        Files.walk(p).filter(f -> f.toString().contains("message")).forEach(f -> {
            File file = f.toFile();
            System.out.println(file.getName());
            Properties properties = new Properties();
            try {
                properties.load(new FileInputStream(file));
                String json = new PropertiesToJsonConverter().convertToJson(properties);
                json = removeUTFCharacters(json).toString();
                String lang = file.getName().replace("message-", "").replace(".properties", "").substring(0, 2).replace("sw", "sv");
                Path newFile = root.resolve(lang).resolve("messages.json");
                File file2 = newFile.toFile();
                file2.getParentFile().mkdirs();
                //write data
                try {
                    Files.write(newFile, json.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public static StringBuffer removeUTFCharacters(String data) {
        Pattern p = Pattern.compile("\\\\u(\\p{XDigit}{4})");
        Matcher m = p.matcher(data);
        StringBuffer buf = new StringBuffer(data.length());
        while (m.find()) {
            String ch = String.valueOf((char) Integer.parseInt(m.group(1), 16));
            m.appendReplacement(buf, Matcher.quoteReplacement(ch));
        }
        m.appendTail(buf);
        return buf;
    }

    public static void main(String[] args) throws IOException {
        //IXOMigration.migrateLocators();
        IXOMigration.migrateMesasges();
    }
}
