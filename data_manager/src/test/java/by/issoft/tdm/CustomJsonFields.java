package by.issoft.tdm;

import by.issoft.tdm.domain.schema.SchemaItem;
import by.issoft.tdm.utils.SchemaExtractor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created 3/23/2020
 *
 * @author <a href="mailto:ArtemMoroz@coherentsolutions.com">Artem Moroz</a>
 * @version 1.0
 */
public class CustomJsonFields {

    public static void main(String[] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        File f = new File("C:\\Projects\\job\\tdm\\sample_data\\manual.json");
        JsonNode node = mapper.readTree(f);

        SchemaItem item = new SchemaExtractor().getSchemaForNodes(Arrays.asList(node), "root");
        System.out.println(item);
    }
}
